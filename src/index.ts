import { MikroORM } from "@mikro-orm/core";
import express from "express";
import path from "path";
import microConfig from "./mikro-orm.config";

const main = async () => {
    const orm = await MikroORM.init(microConfig);
    await orm.getMigrator().up();

    const app = express();
    app.use(express.static(path.join(__dirname, "/pages/public/")));
    app.get('/', (_, res) => {
        res.sendFile(path.join(__dirname, "pages/index.html"));
    });

    app.get('/feide-auth', (req, res) => {
        console.log(req.query);
        // Get feide auth code here and validate it

        res.send("You will be redirected...");
        // Get user data from feide api, then send them back to home page with feide ID
        // and feide name in URL encoded parameters for the client to store
    });

    app.listen(4000, () => {
        console.log("Listening on localhost:4000");
    });
}

main();
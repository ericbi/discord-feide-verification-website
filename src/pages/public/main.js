// Encapsulate and keep variables in this scope
function main() {
    var dataFields = window.location.search;
    var keyValuePairs = dataFields.substring(1).split("&");

    // Get values from url query parameters
    var discordValues = {}
    keyValuePairs.forEach(keyvalue => {
        // Split at the FIRST = and keep the rest (value) intact
        // regardless of the amount of = symbols in the value
        split = keyvalue.split("=");
        key = split.shift();
        value = split.join("=");
        discordValues[key] = value;
    });

    checkDiscordCredentials();

    function checkDiscordCredentials() {
        var { discordImg, discordName, discordId } = discordValues;
        if (!(discordImg && discordName && discordId)) {
            [ img, name, id ] = [
                window.localStorage.getItem("discordImg"),
                window.localStorage.getItem("discordName"),
                window.localStorage.getItem("discordId")
            ]
            if (img && name && id) {
                return setDiscordLoggedIn(img, name, id);
            }
            return setDiscordOauth();
        }
    }

    function setDiscordLoggedIn(img, name, id) {
        var mainDiv = document.createElement("div");
        var img = document.createElement("img");
        var name = document.createElement("p");

        img.src = img;
        name.innerText = name;

        mainDiv.appendChild(img);
        mainDiv.appendChild(name);

        document.querySelector("#discordAuthenticationDiv").appendChild(mainDiv);
    }

    function setDiscordOauth() {
        // Puts in a div element with instructions on how to authenticate
        // using discord oauth

    }
}

main();
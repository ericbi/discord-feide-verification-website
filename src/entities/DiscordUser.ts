import { Entity, PrimaryKey, Property, Unique } from "@mikro-orm/core";

@Entity()
export class DiscordUser {

  @PrimaryKey()
  discordId!: number;

  @Property()
  @Unique()
  feideId!: number;

  @Property({ type: "date" })
  createdAt: Date = new Date();

  @Property({ type: "date", onUpdate: () => new Date() })
  updatedAt: Date = new Date();
}
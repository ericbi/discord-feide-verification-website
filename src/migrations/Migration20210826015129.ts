import { Migration } from '@mikro-orm/migrations';

export class Migration20210826015129 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "discord_user" ("discord_id" serial primary key, "feide_id" int4 not null, "created_at" timestamptz(0) not null, "updated_at" timestamptz(0) not null);');
    this.addSql('alter table "discord_user" add constraint "discord_user_feide_id_unique" unique ("feide_id");');
  }
  
}

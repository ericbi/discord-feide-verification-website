import { MikroORM } from "@mikro-orm/core";
import path from "path";
import { DiscordUser } from "./entities/DiscordUser";

export default {
    entities: [DiscordUser],
    migrations: {
        path: path.join(__dirname, "./migrations"),
        pattern: /^[\w-]+\d+\.[tj]s$/,
    },
    dbName: "DiscordAuthenticationDatabase",
    type: "postgresql",
    debug: true,
} as Parameters<typeof MikroORM.init>[0];